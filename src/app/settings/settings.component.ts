import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import * as dialogs from "tns-core-modules/ui/dialogs";

@Component({
    selector: "Settings",
    templateUrl: "./settings.component.html"
})
export class SettingsComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    doLater(fn) { setTimeout(fn, 100)};

    ngOnInit(): void {

        this.doLater(() => {

            let alerta = {};
            let cerrado = "";
            
            dialogs
            .action("Mensaje", "Cancelar", ["Opcion1", "Opcion2"])
            .then(resultado => {
                console.log(`resultado: ${resultado}`);
                if(resultado === "Opcion1") {
                    cerrado = "Cerrado 1"
                    alerta = {title: "Titulo 1", message: "mje 1", OkButtonText: "btn 1", msg: 'Cerrado 1'};
                }
                else if(resultado === "Opcion2"){
                    cerrado = "Cerrado 2"
                    alerta = {title: "Titulo 2", message: "mje 2", OkButtonText: "btn 2", msg: 'Cerrado 2'};    
                }      
                this.doLater(() => dialogs.alert(alerta).then(() => console.log(cerrado)));
            });

        });
    
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }
}
