import { Component, OnInit } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import { Application } from "@nativescript/core";
import * as Toast from 'nativescript-ui-sidedrawer';
import * as camera from "nativescript-camera";

@Component({
    selector: "Home",
    templateUrl: "./home.component.html"
})

export class HomeComponent implements OnInit {

    constructor() {
        // Use the component constructor to inject providers.
    }

    doLater(fn) { setTimeout(fn, 100)};

    ngOnInit(): void {
        const toastOptions: Toast.ToastOptions = { text: "Hello Wordl", duration: Toast.DURATION.SHORT };
        
        this.doLater(() => Toast.show(toastOptions));
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>Application.getRootView();
        sideDrawer.showDrawer();
    }

    onButtonTap(){
        camera.requestCameraPermissions().then(
            function success(){
                const options = { width: 300, height: 300, keepAspectRatio: false, saveToGallery: true};
                camera.takePicture(options).
                then(imageAsset => {
                    console.log("Tanaño: " + imageAsset.option.width + "x" + imageAsset.options.height);
                    console.log("keepAspectRatio: " + imageAsset.options.keepAspectRatios);
                    console.log("Foto fuardada")
                }).catch(err => {
                    console.log("Error ->" + err.message);
                })
            }, 
            function failure() {
                console.log("Permiso de camara no aceptado por el usuario");
            }
        )
    }
}
