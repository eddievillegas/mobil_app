import { Injectable }from '@angular/core';
import { getJSON, request } from "tns-core-modules/http";
const sqlite = require('nativescript-sqlite');

@Injectable()
export class NoticiasService {
    private noticias: Array<string> = [];
    api: string = "https://303c446f.ngrok.io";
    url: string = `${this.api}/favs`;

    constructor(){
        this.getDb(db => {
            db.each("selectt * from logs", 
            (err, fila) => console.log(`Fila: ${fila}`),
            (err, totales) => console.log(`Filas totales: ${totales}`));
        }, () => console.log("error on getDB"));
    }

    getDb(fnOk, fnError) {
        return new sqlite("mi_db_logs", (err, db) => {
            if(err)
                console.error("Error al abrir db!", err);
            else
                db.execSQL("CREATE TABLE IF NOT EXISTS logs (id INTEGER PRIMARY KEY AUTOINCREMENT, texto TEXT)").then((id) => fnOk(db) , error => fnError(error));
        });
    }

    agregar(s: string){
        return request({ 
            url: this.url,
            method: "post", 
            headers: {"Conten-Type": "application/json"}, 
            content: JSON.stringify({nuevo: s})
        });
    }

    favs(){
        return getJSON(this.url);
    }

    buscar(s: string){
        this.getDb(db => {
            db.execSQL("insert into logs (texto) values (?)", [s], 
                (err, id) => console.log("nuevo id: ", id));
            }, () => console.log("error on getDB"));
            return getJSON(`${this.api}/get?q=s`);
        }
    }

}