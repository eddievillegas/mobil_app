const noticias_state_model = require("~/app/domain/noticias-state.model");

describe("reducersNoticias", () => {
    it("should reduce init data", () => {
        const prevState = noticias_state_model.initializeNoticiasState();
        const action = new noticias_state_model.InitMyDataAction("noticias 1", "noticias 2");
        const newState = noticias_state_model.reducerNoticias(prevState, action);
        expect(newState.items.length).toEqual(2);
        expect(newState.items[0].titulo).toEqual("noticia 1");

    });
    it("should reduce new item added", () => {
        const prevState = noticias_state_model.initializeNoticiasState();
        const action = new noticias_state_model.NuevaNoticiaAction(new noticias_state_model.Noticias("notica 3"));
        const newState = noticias_state_model.reducerNoticias(prevState, action);
        expect(newState.items.length).toEqual(1);
        expect(newState.items[0].titulo).toEqual("noticias 3");
    });
});