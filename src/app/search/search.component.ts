import { Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { Store } from "@ngrx/store";
import * as Toast from "nativescript-toasts";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/appiclation";
import { AppState } from "../app.module";
import { Noticia, NuevaNoticiaAction } from '../domain/noticias-state.mode';
import { NoticiasService } from '../domain/noticias.service';
import * as SocialShare from "nativescript-social-share";

@Component({
    selector: "Search",
    templateUrl: "./search.component.html",
    moduleId: module.id
})

export class SearchComponent implements OnInit {

    resultados: Array<string>;
    @ViewChild("layout") layout: ElementRef;

    constructor(
        private noticias: NoticiasService,
        private store: Store<AppState>
    ) {}

    onPull(e){
        console.log(e);
        const pullRefresh = e.object;
        setTimeout(() => {
            this.resultados.push("xxxxxxx");
            pullRefresh.refreshing=false;
        }, 200);
    }

    ngOnInit(): void {
        this.store.select(state => state.noticias.sugerida)
        .subscribe(data => {
            const f = data;
            if(f != null)
                Toast.show({text: "Sugerimos leer:" + f.titulo, duration: Toast.DURATION.SHORT})
        })
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void {
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    onLongPress(s): void {
        SocialShare.shareText("Asunto: compartido desde el curso!");
    }

    buscarAhora(s: string){
        this.noticias.buscar(s).then((r: any) => {
            this.resultados = r;
        }, e => {
            Toast.show({text: "Error en la busqueda", duration: Toast.DURATION.SHORT});
        })
    }
}
